﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAppCore.Data;
using WebAppCore.Models;
using WebAppCore.ViewModels;

namespace WebAppCore.Controllers
{
    public class HomeController : Controller
    {
	    private readonly IRepository _rep;

		public HomeController(IRepository rep)
	    {
			_rep = rep;
	    }

	    [Authorize]
		public async Task<IActionResult> Index()
	    {
			var students = await _rep.GetStudentsAsync();
			ViewData["UserName"] = User.Identity.Name;
			return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

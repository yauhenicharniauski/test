﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataAccessSQL
{
    public class UniverRepository: IRepository
    {
	    private readonly EFContext _db;
	    public UniverRepository(EFContext db)
	    {
			_db = db;
	    }

	    public async Task<ICollection<Student>> GetStudentsAsync()
	    {
		    return await _db.Students.ToListAsync();
	    }

	    public ICollection<Student> GetStudnets()
	    {
		    return _db.Students.ToList();
	    }

	    public Student GetStudentById(int id)
	    {
		    return _db.Students.FirstOrDefault(x => x.ID == id);
	    }

	    public int AddStudent(Student student)
	    {
		    _db.Students.Add(student);
		    _db.SaveChanges();
		    return student.ID;
	    }

	    public User GetUserById(int id)
	    {
			return _db.Users.FirstOrDefault(x => x.ID == id);
		}

	    public User GetUserByEmail(string email)
	    {
		    return _db.Users.FirstOrDefault(x => x.Email == email);
	    }

		public User Authenticate(string email, string password)
	    {
		    if (string.IsNullOrEmpty(email)) return null;

		    var emailLower = email.ToLower();
		    var user = _db.Users.FirstOrDefault(x => x.Email == emailLower);
		    if (user == null) return null;

		    if (user.Password != password) return null;

		    return user;
	    }

	    public User Register(string email, string password)
	    {
		    var user = new User {Email = email.ToLower(), Password = password};
		    _db.Users.Add(user);
		    _db.SaveChanges();
		    return user;
	    }
    }
}
